const store = require('local-storage');

class FormController {
    form(req, res) {
        store.clear();
        res.render('main/form');
    }
    
    postData(req, res) {
        const { project, device, key } = req.body;
        if(project && device && key){
            store.set('project', project);
            store.set('device', device);
            store.set('key', key);
            res.redirect('/app');
        }
    }
}
  
module.exports = new FormController();
  