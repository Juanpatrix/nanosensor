const fetchData = require('../libraries/antares');
const store = require('local-storage');

const data = [
  {
    'm2m:cin': {
      rn: 'cin_8xhTk0fuTzSFSunK',
      ty: 4,
      ri: '/antares-cse/cin-8xhTk0fuTzSFSunK',
      pi: '/antares-cse/cnt-gAi_WGq4SkmGccJI',
      ct: '20211215T231027',
      lt: '20211215T231027',
      st: 0,
      cnf: 'text/plain:0',
      cs: 34,
      con: '{"temperature":31.9,"humidity":69}'
    },
    content: { temperature: 31.9, humidity: 69 }
  },
  {
    'm2m:cin': {
      rn: 'cin_12_a0LZhTL2NFrqU',
      ty: 4,
      ri: '/antares-cse/cin-12_a0LZhTL2NFrqU',
      pi: '/antares-cse/cnt-gAi_WGq4SkmGccJI',
      ct: '20211215T231014',
      lt: '20211215T231014',
      st: 0,
      cnf: 'text/plain:0',
      cs: 34,
      con: '{"temperature":31.4,"humidity":65}'
    },
    content: { temperature: 31.4, humidity: 65 }
  },
  {
    'm2m:cin': {
      rn: 'cin_OM2YY_mWQgqyCuaR',
      ty: 4,
      ri: '/antares-cse/cin-OM2YY_mWQgqyCuaR',
      pi: '/antares-cse/cnt-gAi_WGq4SkmGccJI',
      ct: '20211215T231002',
      lt: '20211215T231002',
      st: 0,
      cnf: 'text/plain:0',
      cs: 34,
      con: '{"temperature":30.8,"humidity":63}'
    },
    content: { temperature: 30.8, humidity: 63 }
  },
  {
    'm2m:cin': {
      rn: 'cin_RmrPzBgTSd6EJYmz',
      ty: 4,
      ri: '/antares-cse/cin-RmrPzBgTSd6EJYmz',
      pi: '/antares-cse/cnt-gAi_WGq4SkmGccJI',
      ct: '20211215T230950',
      lt: '20211215T230950',
      st: 0,
      cnf: 'text/plain:0',
      cs: 34,
      con: '{"temperature":30.1,"humidity":61}'
    },
    content: { temperature: 30.1, humidity: 61 }
  },
  {
    'm2m:cin': {
      rn: 'cin_qYfVGkeQTEqAUKnC',
      ty: 4,
      ri: '/antares-cse/cin-qYfVGkeQTEqAUKnC',
      pi: '/antares-cse/cnt-gAi_WGq4SkmGccJI',
      ct: '20211215T230937',
      lt: '20211215T230937',
      st: 0,
      cnf: 'text/plain:0',
      cs: 34,
      con: '{"temperature":29.6,"humidity":58}'
    },
    content: { temperature: 29.6, humidity: 58 }
  },
  {
    'm2m:cin': {
      rn: 'cin_vKucCKYeS8qXceYj',
      ty: 4,
      ri: '/antares-cse/cin-vKucCKYeS8qXceYj',
      pi: '/antares-cse/cnt-gAi_WGq4SkmGccJI',
      ct: '20211215T230925',
      lt: '20211215T230925',
      st: 0,
      cnf: 'text/plain:0',
      cs: 34,
      con: '{"temperature":29.2,"humidity":58}'
    },
    content: { temperature: 29.2, humidity: 58 }
  }
]

class HomeController {
  
    index(req, res) {
      store.clear();
      res.render('home');
    }

    documentation(req, res) {
      store.clear();
      res.render('documentation');
    }

    about(req, res) {
      store.clear();
      res.render('about');
    }

    async database(req, res) {
      const project = store.get('project');
      const device = store.get('device');
      const key = store.get('key');
      if( project === null || device === null || key === null) {
        res.send(data);
      } else {
        const dataProject = await fetchData(project, device, key);
        res.send(dataProject);
      }
    }

    async app(req, res) {
      const project = store.get('project');
      const device = store.get('device');
      const key = store.get('key');
      if( project === null || device === null || key === null) {
        res.render('main/app', { data: data });
      } else {
        const dataProject = await fetchData(project, device, key);
          res.render('main/app', { data: dataProject });
      }
    }
}
  
module.exports = new HomeController();
  