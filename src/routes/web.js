const express = require("express");
const HomeController = require("../controllers/HomeController");
const FormController = require("../controllers/FormController");

function routes() {
  const router = express.Router();

  //HomeController
  router.get("/", function (req, res) { 
    HomeController.index(req, res);
  });

  router.get("/documentation", function (req, res) { 
    HomeController.documentation(req, res);
  });

  router.get("/about", function (req, res) { 
    HomeController.about(req, res);
  });

  router.get("/app", function (req, res) { 
    HomeController.app(req, res);
  });

  router.get('/data', function (req, res) { 
    HomeController.database(req, res);
  });

  router.get('/form', function (req, res) { 
    FormController.form(req, res);
  });

  router.post('/form', function (req, res) { 
    FormController.postData(req, res);
  });

  return router;
}

module.exports = routes;
