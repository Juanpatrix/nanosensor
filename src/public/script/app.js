//label X
let labelX = [];

//label Y Temp and Humi
let labelAY1 = [];
let labelAY2 = [];


//menghitung jumlah/banyaknya data
function maxCount(data) {
    let count = 0;

    for (let i = 0; i < data.length; i++) {
        count += 1;
    }

    return count + 1;
}

//Jenis Grafik
function chart() {
    const graph1 = document.getElementById('TemperatureGraph');
    graph1.innerHTML = `<canvas id="ChartTemp" width="100" height="50"></canvas>`;
    const ctx1 = document.getElementById('ChartTemp');
    const tChart = new Chart(ctx1, {
        type: 'line',
        data: {
            labels: labelX,
            datasets: [{
                label: 'Temperature',
                data: labelAY1,
                fill: false,
                borderColor: 'red',
                borderWidth: 1
            }, ]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

    const graph2 = document.getElementById('HumidityGraph');
    graph2.innerHTML = `<canvas id="ChartHumi" width="100" height="50"></canvas>`;
    const ctx2 = document.getElementById('ChartHumi');
    const hChart = new Chart(ctx2, {
        type: 'line',
        data: {
            labels: labelX,
            datasets: [{
                label: 'Humidity',
                data: labelAY2,
                fill: false,
                borderColor: 'blue',
                borderWidth: 1
            }, ]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

}

const fetchAllDataPort = async function () {
    try {
        // const response = await fetch("https://thermal-io.herokuapp.com/data");
        // const data = await response.json();

        // const dataChart = data.map((obj) => ({
        //     temperature: obj.content.temperature, //suhu
        //     humidity: obj.content.humidity //humidity
        // }));

        let temperatures = document.getElementsByClassName("temperature");
        let humidities = document.getElementsByClassName("humidity");

        const dataChart = [];

        for(let i = 0; i < temperatures.length; i++) {
            dataChart[i] = {
                temperature: parseFloat(temperatures[i].outerText),
                humidity: parseFloat(humidities[i].outerText)
            };
        }

        for (let i = 1; i < maxCount(dataChart); i++) {
            labelX.push(i);
        }

        for (let i = dataChart.length-1; i >= 0; i--) {
            labelAY1.push(dataChart[i].temperature);
            labelAY2.push(dataChart[i].humidity);
        }
        chart();
    } catch (err) {
        console.error(err);
    }
};

$(document).ready(function () {
    fetchAllDataPort();
})