/**
 * Source Code: https://goodcalculators.com/dew-point-calculator/
 * 
 */

function mrnd(e) {
    return "NaN" === String(e) ? "-" : 0 == e ? 0 : e % 1 != 0 ? Math.round(e * 100.0) / 100.0 : e
}

function convertFtoC(e) {
    var t;
    return t = .55556 * (e - 32)
}

function convertFtoK(e) {
    var t;
    return t = .55556 * (e - 32) + 273.15
}

function convertCtoF(e) {
    var t;
    return t = 1.8 * e + 32
}

function CalcHeatIndex(e, t) {
    var r = 0,
        s = 0,
        a = Math.log(t / 100),
        n = 17.27 * e / (237.3 + 1 * e);
    r = (n + a) / 17.27;
    var c = r;
    return s = 237.3 * c / (1 - c), convertCtoF(s)
}

function calc(a, s) {
    var e, t, r;

    e = 1 * CalcHeatIndex(a, s);
    t = 1 * convertFtoC(e);
    r = 1 * convertFtoK(e);

    document.getElementById("pe").innerHTML = mrnd(e);
    document.getElementById("pe1").innerHTML = mrnd(t);
    document.getElementById("pe2").innerHTML = mrnd(r);

    if (isNaN(e)) {
        console.log("The Result was :: ", e);
    } else {
        var u = [];
        $(".wmph").each(function () {
            var e = $(this).text();
            u.push(Number(e))
        });
        var i, d = u.reduce(function (e, t) {
                return Math.abs(t - s) < Math.abs(e - s) ? t : e
            }),
            w = e.toFixed(0),
            o = Number(w),
            l = [],
            v = [],
            m = [],
            f = [],
            b = [],
            C = [],
            M = [],
            N = [],
            x = [],
            p = [],
            q = [],
            g = [],
            I = [];

        switch ($(".wchres").each(function () {
            var e = $(this).text();
            Number(e) == Number(i) ? $(this).hide() : $(this).show()
        }), d) {
            case 40:
                $("#wchart .wchres").removeClass("wsel"), $("#wchart tr").find("td.wchres:eq(0)")
                    .each(function () {
                        var e = $(this).text();
                        l.push(e)
                    });
                var i = l.reduce(function (e, t) {
                    return Math.abs(t - o) < Math.abs(e - o) ? t : e
                });
                $("#wchart tr").find("td.wchres:eq(0)").each(function () {
                    var e = $(this).text();
                    Number(e) == Number(i) ? $(this).addClass("wsel") : $(this).removeClass(
                        "wsel")
                });
                break;
            case 45:
                $("#wchart .wchres").removeClass("wsel"), $("#wchart tr").find("td.wchres:eq(1)")
                    .each(function () {
                        var e = $(this).text();
                        v.push(e)
                    });
                var i = v.reduce(function (e, t) {
                    return Math.abs(t - o) < Math.abs(e - o) ? t : e
                });
                $("#wchart tr").find("td.wchres:eq(1)").each(function () {
                    var e = $(this).text();
                    Number(e) == Number(i) ? $(this).addClass("wsel") : $(this).removeClass(
                        "wsel")
                });
                break;
            case 50:
                $("#wchart .wchres").removeClass("wsel"), $("#wchart tr").find("td.wchres:eq(2)")
                    .each(function () {
                        var e = $(this).text();
                        m.push(e)
                    });
                var i = m.reduce(function (e, t) {
                    return Math.abs(t - o) < Math.abs(e - o) ? t : e
                });
                $("#wchart tr").find("td.wchres:eq(2)").each(function () {
                    var e = $(this).text();
                    Number(e) == Number(i) ? $(this).addClass("wsel") : $(this).removeClass(
                        "wsel")
                });
                break;
            case 55:
                $("#wchart .wchres").removeClass("wsel"), $("#wchart tr").find("td.wchres:eq(3)")
                    .each(function () {
                        var e = $(this).text();
                        f.push(e)
                    });
                var i = f.reduce(function (e, t) {
                    return Math.abs(t - o) < Math.abs(e - o) ? t : e
                });
                $("#wchart tr").find("td.wchres:eq(3)").each(function () {
                    var e = $(this).text();
                    Number(e) == Number(i) ? $(this).addClass("wsel") : $(this).removeClass(
                        "wsel")
                });
                break;
            case 60:
                $("#wchart .wchres").removeClass("wsel"), $("#wchart tr").find("td.wchres:eq(4)")
                    .each(function () {
                        var e = $(this).text();
                        b.push(e)
                    });
                var i = b.reduce(function (e, t) {
                    return Math.abs(t - o) < Math.abs(e - o) ? t : e
                });
                $("#wchart tr").find("td.wchres:eq(4)").each(function () {
                    var e = $(this).text();
                    Number(e) == Number(i) ? $(this).addClass("wsel") : $(this).removeClass(
                        "wsel")
                });
                break;
            case 65:
                $("#wchart .wchres").removeClass("wsel"), $("#wchart tr").find("td.wchres:eq(5)")
                    .each(function () {
                        var e = $(this).text();
                        C.push(e)
                    });
                var i = C.reduce(function (e, t) {
                    return Math.abs(t - o) < Math.abs(e - o) ? t : e
                });
                $("#wchart tr").find("td.wchres:eq(5)").each(function () {
                    var e = $(this).text();
                    Number(e) == Number(i) ? $(this).addClass("wsel") : $(this).removeClass(
                        "wsel")
                });
                break;
            case 70:
                $("#wchart .wchres").removeClass("wsel"), $("#wchart tr").find("td.wchres:eq(6)")
                    .each(function () {
                        var e = $(this).text();
                        M.push(e)
                    });
                var i = M.reduce(function (e, t) {
                    return Math.abs(t - o) < Math.abs(e - o) ? t : e
                });
                $("#wchart tr").find("td.wchres:eq(6)").each(function () {
                    var e = $(this).text();
                    Number(e) == Number(i) ? $(this).addClass("wsel") : $(this).removeClass(
                        "wsel")
                });
                break;
            case 75:
                $("#wchart .wchres").removeClass("wsel"), $("#wchart tr").find("td.wchres:eq(7)")
                    .each(function () {
                        var e = $(this).text();
                        N.push(e)
                    });
                var i = N.reduce(function (e, t) {
                    return Math.abs(t - o) < Math.abs(e - o) ? t : e
                });
                $("#wchart tr").find("td.wchres:eq(7)").each(function () {
                    var e = $(this).text();
                    Number(e) == Number(i) ? $(this).addClass("wsel") : $(this).removeClass(
                        "wsel")
                });
                break;
            case 80:
                $("#wchart .wchres").removeClass("wsel"), $("#wchart tr").find("td.wchres:eq(8)")
                    .each(function () {
                        var e = $(this).text();
                        x.push(e)
                    });
                var i = x.reduce(function (e, t) {
                    return Math.abs(t - o) < Math.abs(e - o) ? t : e
                });
                $("#wchart tr").find("td.wchres:eq(8)").each(function () {
                    var e = $(this).text();
                    Number(e) == Number(i) ? $(this).addClass("wsel") : $(this).removeClass(
                        "wsel")
                });
                break;
            case 85:
                $("#wchart .wchres").removeClass("wsel"), $("#wchart tr").find("td.wchres:eq(9)")
                    .each(function () {
                        var e = $(this).text();
                        p.push(e)
                    });
                var i = p.reduce(function (e, t) {
                    return Math.abs(t - o) < Math.abs(e - o) ? t : e
                });
                $("#wchart tr").find("td.wchres:eq(9)").each(function () {
                    var e = $(this).text();
                    Number(e) == Number(i) ? $(this).addClass("wsel") : $(this).removeClass(
                        "wsel")
                });
                break;
            case 90:
                $("#wchart .wchres").removeClass("wsel"), $("#wchart tr").find("td.wchres:eq(10)")
                    .each(function () {
                        var e = $(this).text();
                        q.push(e)
                    });
                var i = q.reduce(function (e, t) {
                    return Math.abs(t - o) < Math.abs(e - o) ? t : e
                });
                $("#wchart tr").find("td.wchres:eq(10)").each(function () {
                    var e = $(this).text();
                    Number(e) == Number(i) ? $(this).addClass("wsel") : $(this).removeClass(
                        "wsel")
                });
                break;
            case 95:
                $("#wchart .wchres").removeClass("wsel"), $("#wchart tr").find("td.wchres:eq(11)")
                    .each(function () {
                        var e = $(this).text();
                        g.push(e)
                    });
                var i = g.reduce(function (e, t) {
                    return Math.abs(t - o) < Math.abs(e - o) ? t : e
                });
                $("#wchart tr").find("td.wchres:eq(11)").each(function () {
                    var e = $(this).text();
                    Number(e) == Number(i) ? $(this).addClass("wsel") : $(this).removeClass(
                        "wsel")
                });
                break;
            case 100:
                $("#wchart .wchres").removeClass("wsel"), $("#wchart tr").find("td.wchres:eq(12)")
                    .each(function () {
                        var e = $(this).text();
                        I.push(e)
                    });
                var i = I.reduce(function (e, t) {
                    return Math.abs(t - o) < Math.abs(e - o) ? t : e
                });
                $("#wchart tr").find("td.wchres:eq(12)").each(function () {
                    var e = $(this).text();
                    Number(e) == Number(i) ? $(this).addClass("wsel") : $(this).removeClass(
                        "wsel")
                })
        }

        const dataSelect = document.getElementsByClassName("wsel")[0].getAttribute("style");
        const response = dataSelect.replace("background-color:#", "")
        const status = document.getElementById("status");
        if (response.includes("fd0100") || response.includes("eb8d0f")) {
            status.innerHTML = `
            <h6 class="text-dark">Status: Very Uncomfortable</h6>
            <p>Pada suhu ${a} <sup>o</sup>C dan kelembapan ${s}% menunjukkan bahwa indikator berbahaya dalam ruangan ditempati dengan persentase udara kelembapan di atas 60%. Hal ini berdampak pada proses terganggunya mekanisme kalor dalam tubuh (proses keluar keringat menjadi terganggu) dan sarang jamur, bakteri lebih mudah berkembang. Hal ini memicu alergen, gangguan pernapasan, kulit, dan lain-lain.  </p>
            `;
        } else if (response.includes("fadf52")) {
            status.innerHTML = `
            <h6 class="text-dark">Status: Uncomfortable</h6>
            <p>Pada suhu ${a} <sup>o</sup>C dan kelembapan ${s}% menunjukkan bahwa ruangan memiliki kelembapan yang cukup tinggi. Hal ini membuat kondisi ruangan menjadi mudah untuk sarang jamur dan bakteri. Akibatnya dapat menimbulkan alergen, alergi, dan bisa terjadi kemungkinan gangguan pernapasan. </p>
            `;
        } else if (response.includes("fdfc86")) {
            status.innerHTML = `
            <h6 class="text-dark">Status: Alright</h6>
            <p>Pada suhu ${a} <sup>o</sup>C dan kelembapan ${s}% menunjukkan bahwa ruangan masih cukup baik, namun perlu diwaspadai terhadap udara yang terlalu lembap (udara basah). </p>
            `;
        } else if (response.includes("cf7")) {
            status.innerHTML = `
            <h6 class="text-dark">Status: Comfortable</h6>
            <p>Pada suhu ${a} <sup>o</sup>C dan kelembapan ${s}% menunjukkan bahwa ruangan masih baik untuk ditempati dengan kondisi suhu ruangan dan kelembapan masih standar. </p>
            `;
        } else if (response.includes("6f1")) {
            status.innerHTML = `
            <h6 class="text-dark">Status: Very Comfortable</h6>
            <p>Pada suhu ${a} <sup>o</sup>C dan kelembapan ${s}% menunjukkan bahwa ruangan yang baik untuk ditempati dengan kondisi kelembapan dan suhu ruangan yang baik. </p>
            `;
        } else if (response.includes("7cf")) {
            status.innerHTML = `
            <h6 class="text-dark">Status: Dry</h6>
            <p>Pada suhu ${a} <sup>o</sup>C dan kelembapan ${s}% menunjukkan bahwa ruangan memiliki udara kering. Maksud dari udara yang kering berarti memiliki kelembapan dibawah rata-rata 40%. Dampak humidity (kelembapan) yang rendah adalah udara lebih cepat memproses melakukan pengangkutan keringat yang artinya kulit menjadi lebih cepat kering. Hal ini berdampak mudahnya kulit mengalami Xerosis atau kondisi kulit mengering akibat proses cepatnya penguapan air keringat pada kulit. Pilihan solusi untuk saat ini menggunakan alat pelembap udara atau humidifier, menggunakan ventilasi, atau exhaust fan untuk membuang udara kering. </p>
            `;
        } else {
            status.innerHTML = `
            <h6 class="text-dark">Status: Unknown</h6>
            <p>Pada suhu ${a} <sup>o</sup>C dan kelembapan ${s}% menunjukkan indikator yang berbahaya bisa mengakibatkan gangguan pernapasan dan pertumbuhan jamur dan bakteri </p>
            `;
        }

    }

}

$(document).ready(function () {
    let temperature = parseInt(1 * document.getElementsByClassName("temperature")[0].innerHTML);
    let humidity = parseInt(1 * document.getElementsByClassName("humidity")[0].innerHTML);
    calc(temperature, humidity);

})