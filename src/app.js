const express = require("express");
const bodyParser = require("body-parser");
const routes = require("./routes/web");
const cors = require("cors");
const ejs = require("ejs");
const { PORT = 3000 } = process.env;
const app = express();

const jsonParser = bodyParser.json();
const urlEncoded = bodyParser.urlencoded({ extended: true });
app.use(cors())
app.use(jsonParser);
app.use(urlEncoded);

app.set("views", "./src/views/pages");
app.set("view engine", "ejs");
app.engine(".ejs", ejs.__express);

app.use("/", routes());
app.use('/static', express.static('./src/public'));

app.listen(PORT, function () {
  console.log(`App running on http://localhost:${PORT}`);
});
