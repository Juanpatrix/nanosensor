const fetch = require("node-fetch");

const fetchData = async (project, device, key) => {
  const requestOptions = {
    method: 'GET',
    headers: {
      "X-M2M-Origin": key,
      "Content-Type": "application/json",
      "Accept": "application/json"
    },
    redirect: 'follow'
  };
  const url = `https://platform.antares.id:8443/~/antares-cse/antares-id/${project}/${device}?fu=1&ty=4&lim=25`;

  const response = await fetch(url, requestOptions);
  const body = await response.text();

  if (body.includes('not found') || body.includes('Unknown')) {
    return [{
      'm2m:cin': {
        rn: 'cin_qYfVGkeQTEqAUKnC',
        ty: 4,
        ri: '/antares-cse/cin-qYfVGkeQTEqAUKnC',
        pi: '/antares-cse/cnt-gAi_WGq4SkmGccJI',
        ct: '                                                 ',
        lt: '                                                 ',
        st: 0,
        cnf: 'text/plain:0',
        cs: 34,
        con: '{"temperature":29.6,"humidity":58}'
      },
      content: { temperature: `Tidak Diketahui`, humidity: `Tidak Diketahui` }
    }];
  } else {
    const data = JSON.parse(body)['m2m:uril'];
    let finalDataArray = [];
    const optionsInd = {
      headers: {
        'X-M2M-Origin': key, // The access key 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      method: 'GET'
    }
    for (let i = 0; i < data.length; i++) {
      const response = await fetch(`https://platform.antares.id:8443/~${data[i]}`, optionsInd);
      const temp = await response.text();
      finalDataArray.push(JSON.parse(temp));
      finalDataArray[i].content = JSON.parse(finalDataArray[i]['m2m:cin'].con);
    }
    // console.log(finalDataArray)
    // console.log(finalDataArray[0].content.temperature);  
    // console.log(finalDataArray[0]['m2m:cin'].ct); 
    return finalDataArray;
  }


}

module.exports = fetchData;

// antares.send(data, 'SmartWiFi', 'ESP32')
// .then((response) => {
//   console.log(response);
// });